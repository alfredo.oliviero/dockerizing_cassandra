# Use the official Cassandra 4.1.3 image
FROM cassandra:4.1.3

# Install gettext to use envsubst
# RUN apt-get update && apt-get install -y iputils-ping less locate gettext-base

# Environment variables to configure Cassandra
ENV CASSANDRA_CLUSTER_NAME=TestCluster
ENV CASSANDRA_NUM_TOKENS=256
ENV CASSANDRA_DC=DC1
ENV CASSANDRA_RACK=RAC1

# Create directory for dump files
# RUN mkdir -p /dump/snapshot

# Copy the CQL script to create the keyspace and tables
# COPY data/dumps/schema/dev_keyspace_1_schema.cql /dump/dev_keyspace_1_schema.cql

# Copy the setup script

# Copy cassandra.yaml and cassandra-rackdc.properties
COPY cassandra.yaml /etc/cassandra/
# COPY cassandra-rackdc.properties /etc/cassandra/

# Substitute environment variables in cassandra.yaml
# RUN envsubst < /etc/cassandra/cassandra.yaml > /etc/cassandra/cassandra.yaml.tmp && mv /etc/cassandra/cassandra.yaml.tmp /etc/cassandra/cassandra.yaml

# Substitute environment variables in cassandra-rackdc.properties
# RUN envsubst < /etc/cassandra/cassandra-rackdc.properties > /etc/cassandra/cassandra-rackdc.properties.tmp && mv /etc/cassandra/cassandra-rackdc.properties.tmp /etc/cassandra/cassandra-rackdc.properties

# COPY scripts/setup.sh /setup.sh
# VOLUME "/scripts/setup.sh"

# Set the entrypoint
# ENTRYPOINT ["/scripts/setup.sh"]

# Expose Cassandra ports
EXPOSE 7000 7001 7199 9042 9160
