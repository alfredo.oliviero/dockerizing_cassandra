# gCube System - Dockerized Cassanra

This is the dockerization of the cassandra infrastructure

## Version

version `v1.0.0`

## Structure of the project

it contains:

* the [script](dump.sh) for dumping the devel environment, and
* the [Dockerfile](./Dockerfile)
* this [Docker compose](./compose.yaml) files to build and start the composed instance with the dumped schema and data

## Built With

* [Docker](https://www.docker.com/)

## Documentation

See [INSTRUCTIONS](INSTRUCTIONS.md)

## Change log

See [CHANGELOG](CHANGELOG.md)

## Authors

* **Alfredo Oliviero** ([ORCID]( https://orcid.org/0009-0007-3191-1025)) - [ISTI-CNR Infrascience Group](https://www.isti.cnr.it/People/A.Oliviero)

## Maintainers

* **Alfredo Oliviero** ([ORCID]( https://orcid.org/0009-0007-3191-1025)) - [ISTI-CNR Infrascience Group](https://www.isti.cnr.it/People/A.Oliviero)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework

This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.

The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)
