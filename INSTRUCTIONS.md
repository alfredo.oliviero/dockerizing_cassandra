## dump:

dump requires nodetool
in prod, nodetool is in `/usr/bin/`

in dev environment, cassandra has been installed manually and nodetool is not in the path

* dev should be aliegned to prod

https://medium.com/@kayvan.sol2/deploying-apache-cassandra-cluster-3-nodes-with-docker-compose-3634ef8345e8


## DOCKER instructions

aggiungere a /etc/host l'entry:

127.0.0.1 cassandra

start composer:
`docker compose up --build`

force recreate docker image

`docker compose build --no-cache`

check  status:
`docker exec -it cassandra1 nodetool status`

## import db

`./dump.sh`

`docker compose up --build`

attendere che tutto sia pronto e i db siano sincronizzati

TODO: definire esattamente come, tendenzialmente con nodetool status , nodetool gossip, etc

eseguire uno dopo l'altro, quando sono terminati
```sh
docker exec -it cassandra1 /scripts/setup.sh
docker exec -it cassandra2 /scripts/setup.sh
docker exec -it cassandra3 /scripts/setup.sh
```

flush before switch off

```sh
docker exec -it cassandra1 nodetool flush
docker exec -it cassandra1 nodetool flush
docker exec -it cassandra1 nodetool flush
```

run a single service:
* cassandra1: `docker compose up cassandra1 --build`
* cassandra2: `docker compose up cassandra2 --build`
* ,...

cqlsh client:
 `docker exec -it cassandra1 cqlsh cassandra1 -k dev_keyspace_1`

open bash on server

* cassandra1: `docker exec -it cassandra1 bin/bash`
* cassandra2: `docker exec -it cassandra2 /bin/bash`
* cassandra3: `docker exec -it cassandra3 /bin/bash`

## check cassandra status

Check status
`nodetool status`



Check if the Gossip protocol is enabled
`nodetool info | grep -i gossip`

Check the status of the Gossip protocol
`nodetool gossipinfo`

Check the communication between nodes
`nodetool ring`

## Documentation

* [cassandra dump data](docs/dump.md)
* [cassandra setup and import](docs/setup.md)
