#!/bin/bash
IP_ADDRESS=$(hostname -I | awk '{print $1}')

# Define a logging function
log() {
    local MESSAGE="$1"
    echo -e "$MESSAGE" | tee -a /var/log/cassandra/is_cassandra_ready.log
}

log "Checking if Cassandra is ready..."

is_cassandra_ready() {
    cqlsh $IP_ADDRESS -e 'SHOW HOST' > /dev/null 2>&1
}

is_cassandra_ready
if [ $? -eq 0 ]; then
    log "Cassandra is ready."
    exit 0
else
    log "Cassandra is not ready."
    exit 1
fi
