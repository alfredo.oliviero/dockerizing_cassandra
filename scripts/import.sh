#!/bin/bash


SNAPSHOT_DIR="$CASSANDRA_DUMP_DIR/snapshot"
IP_ADDRESS=$(hostname -I | awk '{print $1}')

# Define a logging function
log() {
    local MESSAGE="$1"
    echo -e "$MESSAGE" | tee -a /var/log/cassandra/import.log
}

log "Importing snapshots using sstableloader..."
for TABLE_DIR in $(ls $SNAPSHOT_DIR); do
    TABLE_NAME=$(basename $TABLE_DIR) # Extract table name from directory name
    log "Importing table: $TABLE_NAME from directory: $SNAPSHOT_DIR/$TABLE_DIR"
    sstableloader -d "$CASSANDRA_SEEDS" -v -k "$CASSANDRA_KEYSPACE" "$SNAPSHOT_DIR/$TABLE_DIR"
    cqlsh $IP_ADDRESS -k "$CASSANDRA_KEYSPACE" -e "select count(*) from $TABLE_NAME;" >&2
done
