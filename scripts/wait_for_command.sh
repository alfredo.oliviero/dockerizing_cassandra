#!/bin/bash

COMMAND="$1"
TIMEOUT="$2"
SLEEP_DURATION=5  # Sleep duration in seconds for waits

log() {
    local MESSAGE="$1"
    echo -e "$MESSAGE" | tee -a /var/log/cassandra/setup.log
}

wait_for_command() {
    local START_TIME=$(date +%s)
    local END_TIME=$((START_TIME + TIMEOUT))

    while true; do
        if eval "$COMMAND"; then
            log "Command succeeded: $COMMAND"
            break
        else
            local CURRENT_TIME=$(date +%s)
            if [ "$CURRENT_TIME" -ge "$END_TIME" ]; then
                log "Timed out waiting for command: $COMMAND"
                exit 1
            fi
            sleep $SLEEP_DURATION
        fi
    done
}

wait_for_command
