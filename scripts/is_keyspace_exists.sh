#!/bin/bash

# Usage: is_keyspace_exists.sh [--keyspace <keyspace>]
# Example: is_keyspace_exists.sh --keyspace dev_keyspace_1

CASSANDRA_KEYSPACE=${CASSANDRA_KEYSPACE:-}

# Parse arguments
while [ $# -gt 0 ]; do
  case "$1" in
    --keyspace)
      CASSANDRA_KEYSPACE="$2"
      shift 2
      ;;
    *)
      echo "Unknown argument: $1"
      exit 1
      ;;
  esac
done

# Check for required arguments or environment variables
if [ -z "$CASSANDRA_KEYSPACE" ]; then
  echo "CASSANDRA_KEYSPACE is not set. Set it via --keyspace or CASSANDRA_KEYSPACE environment variable."
  exit 1
fi

IP_ADDRESS=$(hostname -I | awk '{print $1}')

if cqlsh $IP_ADDRESS -e "DESCRIBE CASSANDRA_KEYSPACE $CASSANDRA_KEYSPACE;" > /dev/null 2>&1; then
    echo "Keyspace $CASSANDRA_KEYSPACE EXISTS"
    exit 0
fi
echo "Keyspace $CASSANDRA_KEYSPACE DOES NOT EXIST"
exit 1
