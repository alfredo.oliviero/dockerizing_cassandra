#!/bin/bash

IP_ADDRESS=$(hostname -I | awk '{print $1}')
SEEDS=(${CASSANDRA_SEEDS//,/ })


# Define a logging function
log() {
    local MESSAGE="$1"
    echo -e "$MESSAGE" | tee -a /var/log/cassandra/is_schema_agreed.log
}

log "Checking if schema is agreed..."

is_schema_agreed() {
    if cqlsh $IP_ADDRESS -e "DESCRIBE CASSANDRA_KEYSPACE $CASSANDRA_KEYSPACE;" > /dev/null 2>&1; then
        SCHEMA_NODES=$(nodetool describecluster | grep -A 1 "Schema versions:" | grep -o '\[.*\]' | tr -d '[]' | tr ',' '\n' | wc -l)
        if [ "$SCHEMA_NODES" -eq "${#SEEDS[@]}" ]; then
            return 0
        fi
    fi
    return 1
}

is_schema_agreed
if [ $? -eq 0 ]; then
    log "Schema is agreed."
    exit 0
else
    log "Schema is not agreed."
    exit 1
fi
