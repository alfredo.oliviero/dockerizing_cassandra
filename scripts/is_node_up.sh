#!/bin/bash

# Define a logging function
log() {
    local MESSAGE="$1"
    echo -e "$MESSAGE" | tee -a /var/log/cassandra/is_node_up.log
}

# Default values
NODE=${1:-$(hostname -I | awk '{print $1}')}
CASSANDRA_RPC_ADDRESS=${2:-$CASSANDRA_RPC_ADDRESS}

log "Checking if node $NODE is up..."

is_node_up() {
    local NODE="$1"
    local NODE_STATUS=$(nodetool status -r)
    if echo "$NODE_STATUS" | grep -E "^UN" | grep "$NODE" > /dev/null; then
        return 0
    elif [ "$NODE" = "$CASSANDRA_RPC_ADDRESS" ]; then
        NODE_STATUS=$(nodetool status)
        if echo "$NODE_STATUS" | grep -E "^UN.*$(hostname -I | awk '{print $1}')" > /dev/null; then
            return 0
        fi
    fi
    return 1
}

is_node_up $NODE
if [ $? -eq 0 ]; then
    log "Node $NODE is up."
    exit 0
else
    log "Node $NODE is not up."
    exit 1
fi
