#!/bin/bash

# Define a logging function
log() {
    local MESSAGE="$1"
    echo -e "$MESSAGE" | tee -a /var/log/cassandra/setup.log
}

log "RUNNING SETUP"

# Configuration
CASSANDRA_KEYSPACE=${CASSANDRA_KEYSPACE:-dev_keyspace_1}
CASSANDRA_DUMP_DIR=${CASSANDRA_DUMP_DIR:-/dump}  # Ensure CASSANDRA_DUMP_DIR is defined
CASSANDRA_SEEDS=${CASSANDRA_SEEDS:-cassandra1,cassandra2,cassandra3}
PRIMARY_NODE=${PRIMARY_NODE:-false}  # Default to false if not set

IP_ADDRESS=$(hostname -I | awk '{print $1}')

SCHEMA_PATH="$CASSANDRA_DUMP_DIR/schema/${CASSANDRA_KEYSPACE}_schema.cql"
DATA_DIR="/var/lib/cassandra/data/$CASSANDRA_KEYSPACE"
SNAPSHOT_DIR="$CASSANDRA_DUMP_DIR/snapshot"

# Initialize SEEDS array
SEEDS=(${CASSANDRA_SEEDS//,/ })

SLEEP_DURATION=5  # Sleep duration in seconds for waits
TIMEOUT=300  # Timeout in seconds for waits

# Function to wait for a command to succeed
wait_for_command() {
    local COMMAND="$1"
    local TIMEOUT="$2"
    local START_TIME=$(date +%s)
    local END_TIME=$((START_TIME + TIMEOUT))

    while true; do
        if eval "$COMMAND"; then
            log "Command succeeded: $COMMAND"
            break
        else
            local CURRENT_TIME=$(date +%s)
            if [ "$CURRENT_TIME" -ge "$END_TIME" ]; then
                log "Timed out waiting for command: $COMMAND"
                exit 1
            fi

            log "Command failed: $COMMAND, still waiting"
            sleep $SLEEP_DURATION
        fi
    done
}

log "setup CASSANDRA_KEYSPACE: $CASSANDRA_KEYSPACE"
log "setup CASSANDRA_DUMP_DIR: $CASSANDRA_DUMP_DIR"
log "setup SCHEMA_PATH: $SCHEMA_PATH"
log "setup CASSANDRA_SEEDS: $CASSANDRA_SEEDS"

# Check if the keyspace directory exists and is not empty
if [ -d "$DATA_DIR" ] && [ "$(ls -A $DATA_DIR)" ]; then
    log "Data directory $DATA_DIR exists and is not empty. Skipping schema creation and data import."
    EMPTY_DB=false
else
    log "Data directory $DATA_DIR does not exist or is empty. Proceeding with schema creation and data import."
    EMPTY_DB=true
fi

# # Wait for cassandra1 to be ready if this is not the primary node
# if [ "$PRIMARY_NODE" != "true" ]; then
#     wait_for_command "/scripts/is_node_up.sh --node cassandra1 --cassandra_rpc_address $IP_ADDRESS" $TIMEOUT
# fi

# Start Cassandra in the background
cassandra -R &

# Wait for Cassandra to be ready
wait_for_command "/scripts/is_cassandra_ready.sh" $TIMEOUT

# Log the value of PRIMARY_NODE for debugging
log "PRIMARY_NODE is set to: $PRIMARY_NODE"

# Step 1: Wait for all nodes to be up and ready
log "Waiting for all nodes to be up and ready..."
wait_for_command "/scripts/is_node_up.sh --node $seed " $TIMEOUT
// TODO: aspettare tutti i nodi

# Function to wait for all nodes to be up
wait_for_all_nodes_up() {
    for seed in "${SEEDS[@]}"; do
        wait_for_command "/scripts/is_node_up.sh --node $seed " $TIMEOUT
    done
    log "All nodes are up."
}

wait_for_all_nodes_up

# Step 2: Create keyspace and schema on the primary node
if [ "$PRIMARY_NODE" = "true" ]; then
    log "Checking if keyspace $CASSANDRA_KEYSPACE exists..."
    if ! /scripts/is_keyspace_exists.sh --keyspace "$CASSANDRA_KEYSPACE"; then
        log "Keyspace $CASSANDRA_KEYSPACE does not exist. Creating keyspace and tables..."
        cqlsh $IP_ADDRESS -f "$SCHEMA_PATH"
    else
        log "Keyspace $CASSANDRA_KEYSPACE already exists. Ensuring tables exist..."
    fi
fi

# Step 3: Wait for schema to be created and agreed upon across all nodes
log "Waiting for schema agreement across all nodes..."
wait_for_command "/scripts/is_schema_agreed.sh --keyspace $CASSANDRA_KEYSPACE --cassandra_seeds $CASSANDRA_SEEDS" $TIMEOUT

# Step 4: Import data using sstableloader if not previously imported
if [ "$EMPTY_DB" = true ]; then
    log "Importing snapshots using sstableloader..."
    /scripts/import.sh --keyspace "$CASSANDRA_KEYSPACE" --CASSANDRA_DUMP_DIR "$SNAPSHOT_DIR" --cassandra_seeds "$CASSANDRA_SEEDS"
fi

log "FINISHED IMPORT"

# Keep the container running
tail -f /dev/null
